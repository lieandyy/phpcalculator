<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;

abstract class HistoryBaseCommand extends Command 
{
    /**
     * @var string
     */
    protected $description;

    public function __construct()
    {
        parent::__construct();
        
        $this->setName('history:' . $this->getCommandName())
            ->setDescription($this->getCommandDescription())
        ;

        $this->setArguments();
        $this->setOptions();
    }

    abstract protected function getCommandName(): string;

    abstract protected function getCommandDescription(): string;

    abstract protected function setArguments();

    abstract protected function setOptions();

    abstract public function handle(): void;
}