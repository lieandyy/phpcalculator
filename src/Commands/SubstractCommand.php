<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Jakmall\Recruitment\Calculator\Commands\BaseCommand;

class SubstractCommand extends BaseCommand
{
    protected function getCommandVerb(): string
    {
        return 'substract';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'substracted';
    }

    protected function getOperator(): string
    {
        return '-';
    }
    
    protected function calculate($number1, $number2)
    {
        return $number1 - $number2;
    }
}