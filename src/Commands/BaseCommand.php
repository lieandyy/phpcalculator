<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Jakmall\Recruitment\Calculator\Models\Log;
use Jakmall\Recruitment\Calculator\History\CommandHistoryDatabaseManager;
use Jakmall\Recruitment\Calculator\History\CommandHistoryFileManager;

abstract class BaseCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    public function __construct()
    {
        parent::__construct();
        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            '%s {numbers* : The numbers to be %s}',
            $commandVerb,
            $this->getCommandPassiveVerb()
        );
        $this->description = $this->getCommandDescription();
        $this->setName($commandVerb)
            ->setDescription($this->description);
        $this->setArguments();
    }

    protected function getCommandDescription()
    {
        $commandVerb = $this->getCommandVerb();
        return sprintf('%s all given Numbers', ucfirst($commandVerb));
    }

    protected function setArguments()
    {
        $this->addArgument('numbers', InputArgument::IS_ARRAY, 'The numbers to be ' . $this->getCommandPassiveVerb());
    }

    abstract protected function getCommandVerb(): string;

    abstract protected function getCommandPassiveVerb(): string;

    public function handle(): void
    {
        $numbers = $this->getInput();
        $description = $this->generateCalculationDescription($numbers);
        $result = $this->calculateAll($numbers);

        $this->saveHistory($description, $result);

        $this->comment(sprintf('%s = %s', $description, $result));
    }

    protected function saveHistory($description, $result)
    {
        $dbManager = new CommandHistoryDatabaseManager();
        $dbManager->log([
            'command' => $this->getCommandVerb(),
            'description' => $description,
            'result' => $result
        ]);
        $fileManager = new CommandHistoryFileManager();
        $fileManager->log([
            'command' => $this->getCommandVerb(),
            'description' => $description,
            'result' => $result,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }

    public function handleForHttp($input)
    {
        $description = $this->generateCalculationDescription($input);
        $result = $this->calculateAll($input);
        $this->saveHistory($description, $result);

        return [
            'command' => $this->getCommandVerb(),
            'operation' => $description,
            'result' => $result
        ];
    }

    protected function getInput(): array
    {
        return $this->argument('numbers');
    }

    protected function generateCalculationDescription(array $numbers): string
    {
        $operator = $this->getOperator();
        $glue = sprintf(' %s ', $operator);

        return implode($glue, $numbers);
    }

    abstract protected function getOperator(): string;

    /**
     * @param array $numbers
     *
     * @return float|int
     */
    protected function calculateAll(array $numbers)
    {
        $number = array_pop($numbers);

        if (count($numbers) <= 0) {
            return $number;
        }

        return $this->calculate($this->calculateAll($numbers), $number);
    }

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    abstract protected function calculate($number1, $number2);
}