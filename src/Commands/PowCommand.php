<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Jakmall\Recruitment\Calculator\Commands\BaseCommand;
use Symfony\Component\Console\Input\InputArgument;

class PowCommand extends BaseCommand
{
    protected function getCommandDescription()
    {
        return 'Exponent the given number';
    }

    protected function setArguments()
    {
        $this->addArgument('base', InputArgument::REQUIRED, 'The base number');
        $this->addArgument('exp', InputArgument::REQUIRED, 'The exponent number');
    }

    protected function getCommandVerb(): string
    {
        return 'pow';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'exponent';
    }

    protected function getOperator(): string
    {
        return '^';
    }

    protected function getInput(): array
    {
        $base = $this->argument('base');
        $exp = $this->argument('exp');

        return [
            $base, $exp
        ];
    }
    
    protected function calculate($number1, $number2)
    {
        return pow($number1, $number2);
    }
}