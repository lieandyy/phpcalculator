<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Jakmall\Recruitment\Calculator\Commands\HistoryBaseCommand;
use Symfony\Component\Console\Input\InputArgument;
use Jakmall\Recruitment\Calculator\History\CommandHistoryDatabaseManager;
use Jakmall\Recruitment\Calculator\History\CommandHistoryFileManager;

class HistoryClearCommand extends HistoryBaseCommand
{
    protected function getCommandName(): string
    {
        return 'clear';
    }

    protected function getCommandDescription(): string
    {
        return 'Clear saved history';
    }

    protected function setArguments() 
    {
        return;
    }

    protected function setOptions()
    {
        return;
    }

    public function handle(): void
    {
        $dbManager = new CommandHistoryDatabaseManager();
        $fileManager = new CommandHistoryFileManager();
        $dbManager->clearAll();
        $fileManager->clearAll();
        $this->comment('History cleared!');
    }
}