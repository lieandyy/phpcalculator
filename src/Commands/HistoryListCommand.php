<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Jakmall\Recruitment\Calculator\Commands\HistoryBaseCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Jakmall\Recruitment\Calculator\History\CommandHistoryDatabaseManager;
use Jakmall\Recruitment\Calculator\History\CommandHistoryFileManager;

class HistoryListCommand extends HistoryBaseCommand
{
    private $manager;

    public function __construct()
    {
        parent::__construct();
    }

    protected function getCommandName(): string
    {
        return 'list';
    }

    protected function getCommandDescription(): string
    {
        return 'Show calculator history';
    }

    protected function setArguments() 
    {
        $this->addArgument('commands', InputArgument::IS_ARRAY, 'Filter the history by commands');
    }

    protected function setOptions()
    {
        $this->addOption(
            'driver',
            'D',
            InputOption::VALUE_OPTIONAL,
            'Driver for storage connection',
            'database'
        );
    }

    public function handle(): void
    {
        $driver = $this->option('driver');
        switch($driver) 
        {
            case 'database':
                $this->manager = new CommandHistoryDatabaseManager();
                break;
            case 'file':
                $this->manager = new CommandHistoryFileManager();
                break;
        }
        $list = $this->manager->findAll();

        if(count($list) <= 0)
        {
            $this->comment('History is empty.');
        }
        else 
        {
            $input = $this->argument('commands');
            $counter = 1;
            $comment = '';
            foreach($list as $i => $l)
            {
                if(count($input) == 0 || in_array($l->command, $input)) 
                {
                    $comment .= "No: $counter\nCommand: " . $l->command 
                                . "\nDescription: " . $l->description 
                                . "\nResult: " . $l->result 
                                . "\nOutput: " . $l->description . " = " . $l->result 
                                . "\nTime: " . $l->created_at . "\n\n"
                    ;
                    ++$counter;
                }
            }

            $this->comment($comment);
        }
    }
}