<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;
use Jakmall\Recruitment\Calculator\History\CommandHistoryDatabaseManager;
use Jakmall\Recruitment\Calculator\History\CommandHistoryFileManager;

class HistoryController
{
    public function index()
    {
        $manager = new CommandHistoryDatabaseManager();
        return $manager->findAll();
        dd('create history logic here');
    }

    public function show($id)
    {
        $manager = new CommandHistoryDatabaseManager();
        return $manager->show($id);
        dd('create show history by id here');
    }

    public function remove($id)
    {
        $dbManager = new CommandHistoryDatabaseManager();
        $fileManager = new CommandHistoryFileManager();
        $fileManager->clear($id);
        $dbManager->clear($id);
        header('HTTP/1.1 203 Non-Authoritative Information');
        http_response_code(203);
        return;
        dd('create remove history logic here');
    }
}
