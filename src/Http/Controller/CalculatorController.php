<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

class CalculatorController
{
    public function calculate($action)
    {
        $request = explode(' ', $action);
        $operator = array_shift($request);

        $command = null;
        switch($operator)
        {
            case 'add':
                $command = new \Jakmall\Recruitment\Calculator\Commands\AddCommand();
                break;
            case 'substract':
                $command = new \Jakmall\Recruitment\Calculator\Commands\SubstractCommand();
                break;
            case 'multiply':
                $command = new \Jakmall\Recruitment\Calculator\Commands\MultiplyCommand();
                break;
            case 'divide':
                $command = new \Jakmall\Recruitment\Calculator\Commands\DivideCommand();
                break;
            case 'pow':
                $command = new \Jakmall\Recruitment\Calculator\Commands\PowCommand();
                break;
        }
        $result = $command->handleForHttp($request);

        return $result;
        dd('calculate result here');
    }
}
