<?php

namespace Jakmall\Recruitment\Calculator\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $hidden = [
        'description',
        'created_at',
        'updated_at'
    ];
    protected $appends = ['operation', 'input', 'time'];

    public function getTimeAttribute() 
    {
        return $this->created_at;
    }

    public function getInputAttribute()
    {
        $operator = explode(' ', $this->description)[1];
        return explode($operator, str_replace(' ', '', $this->description));
    }

    public function getOperationAttribute()
    {
        return $this->description;
    }
}