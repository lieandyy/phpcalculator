<?php

namespace Jakmall\Recruitment\Calculator\History;

use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Jakmall\Recruitment\Calculator\Models\Log;

class CommandHistoryFileManager implements CommandHistoryManagerInterface 
{
    private $fileName = 'history';
    private $delimiter = '#';

    private function convertStringToLog($log)
    {
        $attr = explode($this->delimiter, $log);
        $l = new Log();
        $l->command = $attr[0];
        $l->description = $attr[1];
        $l->result = $attr[2];
        $l->created_at = $attr[3];
        $l->id = $attr[4];
        return $l;
    }

    public function findAll(): array
    {
        try 
        {
            $result = [];
            $f = fopen($this->fileName, 'r');
            $data = @fread($f, filesize($this->fileName));
            $logs = explode("\n", $data);
            foreach($logs as $log)
            {
                if(trim($log) != '') 
                {
                    $l = $this->convertStringToLog($log);
                    array_push($result, $l);
                }
            }
            fclose($f);
            return $result;
        }
        catch(Exception $e)
        {
            return [];
        }
        return [];
    }

    public function log($command): bool
    {
        try 
        {
            $id = count($this->findAll()) + 1;
            $f = fopen($this->fileName, 'a');
            fwrite($f, $command['command'] . $this->delimiter 
                    . $command['description'] . $this->delimiter 
                    . $command['result'] . $this->delimiter 
                    . $command['created_at'] . $this->delimiter
                    . $id . "\n"
            );
            fclose($f);

            return true;
        }
        catch(Exception $e) 
        {
            return false;
        }
        
        return false;
    }

    public function show($id): object
    {
        try
        {
            return array_map(function($l) use($id)
            {
                return $l->id == $id;
            }, $this->findAll());
        }
        catch(Exception $e)
        {
            return null;
        }
    }
    
    public function clearAll(): bool
    {
        try
        {
            $f = fopen($this->fileName, 'w');
            fwrite($f, '');
            fclose($f);

            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
        return false;
    }

    public function clear($id): bool
    {
        try
        {
            $logs = $this->findAll();
            $logs = array_filter($logs, function($l) use($id)
            {
                return $l->id != $id;
            });

            $f = fopen($this->fileName, 'w');
            $logsString = '';

            foreach($logs as $log)
            {
                $logsString .= $log['command'] . $this->delimiter 
                            . $log['description'] . $this->delimiter 
                            . $log['command'] . $this->delimiter 
                            . $log['created_at'] . $this->delimiter
                            . $id . "\n"
                ;
            }

            fwrite($f, $logsString);
            fclose($f);
        }
        catch(Exception $e)
        {
            return false;
        }
        return false;
    }
}