<?php

namespace Jakmall\Recruitment\Calculator\History;

use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Jakmall\Recruitment\Calculator\Models\Log;

class CommandHistoryDatabaseManager implements CommandHistoryManagerInterface 
{
    public function findAll(): array
    {
        $result = [];
        $logs = Log::get();
        foreach($logs as $log) 
        {
            array_push($result, $log);
        }
        return $result;
    }

    public function log($command): bool
    {
        $log = new Log();
        $log->command = $command['command'];
        $log->description = $command['description'];
        $log->result = $command['result'];
        return $log->save();
    }

    public function show($id): object
    {
        return Log::find($id);
    }

    public function clearAll(): bool
    {
        try
        {
            Log::truncate();
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }

    public function clear($id): bool
    {
        try
        {
            Log::destroy($id);
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }
}